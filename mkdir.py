import os
import time

# size = 400000
size = 2002
base_dir = "data"

def main():
    for i in range(1, size+1):
        print(i)
        prefix = str(int(i / 1000)).zfill(3)
        print("prefix", prefix)
        suffix = str(i % 1000).zfill(3)
        print("suffix", suffix)
        
        if not os.path.exists(base_dir+'/'+prefix+'/'+suffix):
            os.makedirs(base_dir+'/'+prefix+'/'+suffix)

if __name__ == '__main__':
    main()
