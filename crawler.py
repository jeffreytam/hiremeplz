#!/usr/bin/python3
# -*- coding: utf-8 -*-
from lxml import html
import requests
import logging
import time
import sys
import pymysql.cursors
from multiprocessing import Pool
from functools import partial
from config import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

cookies = "employer_id=593747; openid=o9fZrwVFMo0INa7LKgECbeLML81w; own_wx_openid=o9fZrwVFMo0INa7LKgECbeLML81w; PHPSESSID=pck4t3l7msj78mhsro6kgdivs0; SERVERID=6443693d5778565271e834d7405e921c|1482398940|1482398863; unionid=oNmE8w25qeFhJeqThjZpCVlAFNWU; user_id=991311; wx_nickname=%E7%9A%AE%E7%89%B9_TVwo5BGE; PHPSESSID=j7oo64v1drncfiposkbrtpvh37; shangjia_tag=1; setviprource=3; Hm_lvt_2782087f580a8237c10130fc701fb588=1482230919,1482398440; Hm_lpvt_2782087f580a8237c10130fc701fb588=1483032388; CNZZDATA1259405286=2062082537-1482397631-http%253A%252F%252Fwx.hiremeplz.com%252F%7C1483028467; Hm_lvt_252f04836478a992417d56b352ee5029=1482395082,1482396528; Hm_lpvt_252f04836478a992417d56b352ee5029=1483032493; start=1; dy=1; SERVERID=6443693d5778565271e834d7405e921c|1483032492|1483032492"

def getNextEmptyUid():
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT MAX(uid) as max FROM (SELECT `uid` FROM `members` UNION ALL SELECT `uid` FROM `non_members`) AS all_members"
            cursor.execute(sql)
            result = cursor.fetchone()
    finally:
        connection.close()

    if (result['max'] == None):
        return 1
    else:
        return result['max'] + 1

def saveData(data):
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            if (data['data'] == 0):
                logger.info("No data found for uid {0}".format(data['uid']))
                sql = "INSERT INTO `non_members` (`id`, `uid`, `crawl_time`) VALUES (NULL, %s, CURRENT_TIMESTAMP)"
                cursor.execute(sql, (data['uid']))
            elif (data['wechat'] == 'qr'):
                # logger.info("Adding uid {0} {1}".format(data['uid'], data['nickname']))
                sql = "INSERT INTO `members` (`id`, `uid`, `nickname`, `gender`, `age`, `height`, `location`, `constellation`, `job`, `income`, `orientation`, `price`, `qr`, `mobile`, `pictures`, `crawl_time`, `pics_crawl_time`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NULL, %s, CURRENT_TIMESTAMP, NULL)"
                cursor.execute(sql, (data['uid'], data['nickname'], data['gender'], data['age'], data['height'], data['location'], data['constellation'], data['job'], data['income'], data['orientation'], data['price'], data['qr'], data['pictures']))
            elif (data['wechat'] == 'mobile'):
                # logger.info("Adding uid {0} {1}".format(data['uid'], data['nickname']))
                sql = "INSERT INTO `members` (`id`, `uid`, `nickname`, `gender`, `age`, `height`, `location`, `constellation`, `job`, `income`, `orientation`, `price`, `qr`, `mobile`, `pictures`, `crawl_time`, `pics_crawl_time`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NULL, %s, %s, CURRENT_TIMESTAMP, NULL)"
                cursor.execute(sql, (data['uid'], data['nickname'], data['gender'], data['age'], data['height'], data['location'], data['constellation'], data['job'], data['income'], data['orientation'], data['price'], data['mobile'], data['pictures']))
        connection.commit()
    except pymysql.IntegrityError as e:
        logger.error(e)
    finally:
        connection.close()


def getMemberProfile(id, session):
    get_start = time.time()

    id = str(id)
    data = {}

    for attempt in range(0, 30):
        if (attempt > 0):
            print("attempt %s" % attempt)
        try:
            response = session.get(
                url="http://wx.hiremeplz.com/index.php",
                params={
                    "s": "Index/chat/appid/wx553c8bc53fd0724c/jobber_id/" + id,
                },
                headers={
                    "Host": "wx.hiremeplz.com",
                    "Upgrade-Insecure-Requests": "1",
                    "Referer": "http://wx.hiremeplz.com/index.php?appid=wx5",
                    "Cookie": cookies,
                    "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) \
                        AppleWebKit/602.3.12 (KHTML, like Gecko) Mobile/14C92 MicroMessenger/6.5.2 NetType/WIFI Language/zh_CN",
                    "Accept-Language": "zh-cn",
                    "Accept-Encoding": "gzip, deflate",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                },
            )
            logger.debug('Status Code: {status_code}'.format(status_code=response.status_code))

            if (response.status_code != 200):            
                continue
            else:
                content=response.content.decode('utf-8')

            tree = html.fromstring(content)

            profile_url = tree.xpath('//div[@class="header"]/a/@href')[0]

            if (profile_url == "./index.php?s=/Index/detail/appid/wx553c8bc53fd0724c/id/"):
                return {'uid': id, 'data': 0}

            data['uid'] = id
            data['data'] = 1
            data['nickname'] = tree.xpath('//div[@class="title"]/text()')[0].strip()[2:]
            style = tree.xpath('//div[@class="info"]/div[2]/img[2]/@style')
            if (style):
                data['wechat'] = 'qr'
                data['qr'] = tree.xpath('//div[@class="info"]/div[2]/img[2]/@src')[0]
            else:
                data['wechat'] = 'mobile'
                data['mobile'] = tree.xpath('//div[@class="info"]/div[2]/text()')[1].strip()[6:]

        except requests.exceptions.RequestException:
            logger.error('HTTP Request failed')
            logger.error(response.status_code)
            logger.error(response.headers)
            time.sleep(request_retry_delay)
            continue
        break

    for attempt in range(0, 30):
        if (attempt > 0):
            print("attempt %s" % attempt)
        try:
            response = session.get(
                url="http://wx.hiremeplz.com/index.php",
                params={
                    "s": "/Index/detail/appid/wx553c8bc53fd0724c/id/" + id,
                },
                headers={
                    "Host": "wx.hiremeplz.com",
                    "Upgrade-Insecure-Requests": "1",
                    "Referer": "http://wx.hiremeplz.com/index.php?appid=wx5",
                    "Cookie": cookies,
                    "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) \
                        AppleWebKit/602.3.12 (KHTML, like Gecko) Mobile/14C92 MicroMessenger/6.5.2 NetType/WIFI Language/zh_CN",
                    "Accept-Language": "zh-cn",
                    "Accept-Encoding": "gzip, deflate",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                },
            )
            logger.debug('Status Code: {status_code}'.format(status_code=response.status_code))

            if (response.status_code != 200):            
                return []
            else:
                content=response.content.decode('utf-8')

            tree = html.fromstring(content)

            location_html = tree.xpath('//div[@class="user_info"]/div/comment()')[0]
            location_subtree = html.fromstring(str(location_html)[4:-3])
            location = location_subtree.xpath('//span/text()')
            data['location'] = location[0] if len(location) > 0 else ''

            price = tree.xpath('//div[@class="user_price"]/text()')
            price = str(price[0]) if len(price) > 0 else '0'
            if ('元/时' in price):
                price = price.replace('元/时', '')
            if ('倒贴' in price):
                price = price.replace('倒贴', '-')
            data['price'] = int(price)

            gender = tree.xpath('//img[@class="detailSex"]/@src')
            gender = gender[0] if len(gender)> 0 else ''
            if ('boy' in gender):
                data['gender'] = 'M'
            elif ('girl' in gender):
                data['gender'] = 'F'
            else:
                data['gender'] = 'U'

            age = tree.xpath('//img[@src="/Public/wechat/img/detail_age.png"]/../span[2]/text()')
            age = age[0] if len(age) > 0 else "0"
            data['age'] = int(age)

            height = tree.xpath('//img[@src="/Public/wechat/img/detail_height.png"]/../span[2]/text()')
            height = height[0] if len(height) > 0 else "0cm"
            height = height.replace('cm', '')
            data['height'] = int(height) if len(height) > 0 else 0

            constellation = tree.xpath('//img[@src="/Public/wechat/img/detail_constellation.png"]/../span[2]/text()')
            data['constellation'] = constellation[0].strip() if len(constellation) > 0 else ''

            job = tree.xpath('//img[@src="/Public/wechat/img/detail_work.png"]/../span[2]/text()')
            data['job'] = job[0].strip() if len(job) > 0 else ''

            income = tree.xpath('//img[@src="/Public/wechat/img/detail_income.png"]/../span[2]/text()')
            data['income'] = income[0].strip().replace('/月', '') if len(income) > 0 else ''

            orientation = tree.xpath('//img[@src="/Public/wechat/img/detail_sex.png"]/../span[2]/text()')
            data['orientation'] = orientation[0].strip() if len(orientation) > 0 else ''

            pictures = tree.xpath('//div[@class="lunboimg"]/a/img/@src')
            data['pictures'] = ' '.join(pictures)

        except requests.exceptions.RequestException:
            logger.error('HTTP Request failed')
            time.sleep(request_retry_delay)
            continue
        break

    time_elapsed = round(time.time() - get_start, 2)
    logger.info("Got data for uid({0}) in {1} secs".format(id, time_elapsed))

    return data


def main():
    s = requests.Session()

    while True:
        time_start = time.time()

        start = getNextEmptyUid()
        logger.info("Starting from uid {0}".format(start))

        if (start + threads > stopsign):
            logger.info("About to reach the last member, exiting")
            break

        pool = Pool(crawl_workers)

        resultset = pool.map(partial(getMemberProfile, session=s), range(start, start + threads))

        pool.close()
        pool.join()

        for res in resultset:
            if ('data' in res):
                saveData(res)
            else:
                logger.debug("Invalid data node")

        time_end = time.time()
        logger.info("Total time elapsed = {0} secs".format(round(time_end - time_start, 2)))
        time.sleep(crawler_rest_time)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print(sys.stderr, '\nExiting by user request\n')
        sys.exit(0)