#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import time
import sys
import os
import requests
import pymysql.cursors
from multiprocessing import Pool
from functools import partial
from config import *


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def getNextDownloadJob():
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT uid FROM members WHERE pics_crawl_time IS NULL LIMIT 1"
            cursor.execute(sql)
            result = cursor.fetchone()
    finally:
        connection.close()

    if (result != None):
        return result['uid']
    else:
        return False


def getPicsData(uid):
    data = {}
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT uid, qr, mobile, pictures FROM members WHERE pics_crawl_time IS NULL LIMIT 1"
            # sql = "SELECT uid, qr, mobile, pictures FROM members WHERE id = " + id
            cursor.execute(sql)
            result = cursor.fetchone()
    finally:
        connection.close()

    if (result != None):
        data = result['pictures'].split()
        if (result['qr'] != None):
            data.extend([result['qr']])

    return data

def updateDownloadStatus(uid):
    uid = str(uid)
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE `members` SET `pics_crawl_time` = CURRENT_TIMESTAMP WHERE `uid` =" + uid
            cursor.execute(sql)
            connection.commit()
    finally:
        connection.close()


def downloadOne(url, uid, path, session):
    download_start = time.time()
    filename = url.split('/')[-1]
    # logger.debug(filename)

    try:
        r = session.get(url, stream=True)
        with open(path+'/'+filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk:
                    f.write(chunk)
    except Exception as e:
        print(e)
        return uid, False

    time_elapsed = round(time.time() - download_start, 2)
    # logger.info("File downloaded in {0} secs".format(time_elapsed))
    return uid, True

def downloadAll(uid, session):
    prefix = str(int(uid / 1000)).zfill(3)
    suffix = str(uid % 1000).zfill(3)
    path = base_dir+'/'+prefix+'/'+suffix
    # logger.debug(path)

    if not os.path.exists(path):
        os.makedirs(path)

    queue = {}
    # s = requests.Session()

    data = getPicsData(uid)

    for i in range(len(data)):
        for attempt in range(0, 50):
            if (attempt > 0):
                print("attempt %s" % attempt)
            try:
                r = session.head(data[i])
                if (r.status_code == 200):
                    size = r.headers['Content-Length']
                    # print(size)
                    if (size not in queue):
                        queue[size] = data[i]
                    else:
                        logger.debug("Skipping duplicate")
                else:
                    # logger.error(data[i])
                    logger.error(r.status_code)
                    break
            except Exception as e:
                logger.error("requests failed")
                logger.error(e)
                time.sleep(request_retry_delay)
                continue
            break

    pool = Pool(download_workers)

    logger.info("Download {0} files for uid({1})".format(len(queue), uid))
    resultset = pool.map(partial(downloadOne, uid=uid, path=path, session=session), queue.values())

    pool.close()
    pool.join() 

    # print(resultset)
    if (False not in resultset):
        updateDownloadStatus(uid)

def main():
    s = requests.Session()
    while True:
        time_start = time.time()
        uid = getNextDownloadJob()
        if (uid):
            # logger.info("Working on uid(%s)" % uid)
            downloadAll(uid, session=s)
            time_end = time.time()
            logger.info("Total time elapsed = {0} secs".format(round(time_end - time_start, 2)))
            time.sleep(download_rest_time)
        else:
            logger.info("No more work to do")
            time.sleep(3600)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print >> sys.stderr, '\nExiting by user request\n'
        sys.exit(0)